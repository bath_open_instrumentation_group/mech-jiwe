### Compiling and running this sketch

#### Preparation
1. Download newest Arduino editor. NOTE: The Ubuntu repositories are really old, so download from Arduino site. Also the install.sh for Arduino is completely mental so install just tries to put "shortcuts" on your "desktop" but doesn't add the IDE to the path! It is much better to just extract to ~/opt and then symlink the executable into ~/bin
1. Install the Arduino DUE board in the arduino editor under Tools -> Board -> Board Manager
1. For the libraries on linux you can run the bash script `linuxlibs.sh` to automatically download all libraries into the correct libraries folder (Restart the ardunion editor after). All remaining steps apply to Windows only
1. Download the following zips:
    1. https://gitlab.com/julianstirling/ArduinoSerialCommand/-/archive/master/1. https://github.com/laurb9/StepperDriver/archive/master.zip
    1. https://github.com/bogde/HX711/archive/master.zip
    1. https://github.com/PaulStoffregen/SoftwareSerial/archive/master.zip
    1. https://github.com/sirleech/NewSoftSerial/archive/master.zip
1. Use the arduino editor to laboriously select Sketch -> Include Library -> Add .ZIP Library, for each indivisual library.

    
#### Compiling
1. Plug the Due into the computer using the 'programming port'
1. Make sure the Due Programming port is selected in Tools->Board
1. Make sure the correct port it selected in Tools->Port. If only one Arduino is plugged in it should be the only option
1. Click the little tick at the top left to verify the code will compile.
1. Click the arrow next to the tick to upload to the device.
1. If everything worked you should be ready to go!


#### Troubleshooting

If everything did not work the problem is probably the arduino libraires or the board/port selection. Check the following:
1. Check every library except Arduino.h is highlighted in orange. Libraries are on lines begining `#include`. If it is not restart the editor and see if this changes. If they are still not orange check you have installed all of the above libraies.
1. Check the correct board is selected.
1. Reselect the port in Tools->Port, sometimes the port will change.
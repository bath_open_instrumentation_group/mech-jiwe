/*
STEPPER MOTOR DRIVER FOR THE MECH JIWE - A SIMPLE MECHANICAL TESTER
This firmware is for the Arduino Due and Arduino CNC Shield V3, where as the X and Y 
stepper motor connections have been used to drive a pair of Stepper Motors for the Z
motion of the Mechanical Tester Unit.

NOTE:
   We are using a modified SerialCommand.h which does not need softwareserial.h. See readme in this folder!
 
Code writen by:  Eng. SANGA, Valerian Linus
         Julian Stirling

Based on code by:
         Rob Eager
Date: July 2018
Licence: GNU GPL V3
 */


#include <Arduino.h>
#include <Servo.h>
#include <A4988.h>
#include <SerialCommand.h> 
#include "HX711.h"

// Motor steps per revolution. Most steppers are 200 steps or 1.8 degrees/step
#define MOTOR_STEPS 200
//STEPPER MOTORS DEFINING
//** Stepper1
#define Stepper1_DIR  5
#define Stepper1_STEP 2
//** Stepper Enable pin
#define Stepper_ENBL  8
//Loadcell I2c pins
#define DOUT 20       //DOT Pin
#define CLK  21      //CLK Pin


//Globals!
SerialCommand myCMD;
A4988 Stepper1(MOTOR_STEPS, Stepper1_DIR, Stepper1_STEP, Stepper_ENBL);
HX711 loadcell(DOUT, CLK);
int RPM;
double calibration_factor =-0.004441;

void setup()
{
  Serial.begin(9600);
  while (!Serial){};
  
  //Setup Stepper motors
  Stepper1.disable();
  RPM = 100;
  Stepper1.setRPM(RPM);
  loadcell.set_scale(1);
  //Reset the loadcell to 0
  loadcell.tare();


  myCMD.addCommand("move", move_motor);
  myCMD.addCommand("set_speed",set_speed);
  myCMD.addCommand("load",print_load);
  myCMD.addCommand("load_raw",print_load_raw);
  myCMD.addCommand("tare",tare);
  myCMD.addDefaultHandler(unrecognised_cmd);      // For unrecognised commands
}

void loop()
{
  myCMD.readSerial();
}

void set_speed()
{
  char *speedin = myCMD.next();
  RPM = atoi(speedin);
  Stepper1.setRPM(RPM);
}

void move_motor()
{
  Stepper1.enable();
  char *steps = myCMD.next();
  Stepper1.move(atoi(steps));
  Serial.println("Move complete.");
}

void print_load()
{
  Serial.println(measure_load(true),14);
}

void print_load_raw()
{
  Serial.println(measure_load(false),14);
}

double measure_load(int cal)
{
  double load = loadcell.get_units();
  if (cal)
  {
    load *=calibration_factor;
  }
  return load;
}

void tare()
{
  loadcell.tare();
  Serial.println("Load cell tared.");
}

void unrecognised_cmd()
{
  Serial.println("Error: Command not recognised");
}

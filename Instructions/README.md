# Instructions

The original build notes are available in a PDF here. The design has changed quite a bit since then. These notes are a bit jumbled and need improving. We are missing some information too.

Sections:

1. [Printing](Printing.md)
1. [Electronics Assembly](ElectronicsAssembly.md)
1. [Running the Software](RunningSoftware.md)


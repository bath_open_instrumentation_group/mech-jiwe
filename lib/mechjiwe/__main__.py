from .interactive_control import keyboard_app


def main():
    keyboard_app()


if __name__ == "__main__":
    main()

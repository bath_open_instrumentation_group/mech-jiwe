# Interactive control for Mech Jiwe
from __future__ import print_function, division

from .mech_jiwe import MechJiwe
from . import keyboard_control_utils as kcu
import argparse
import serial
from serial.tools.list_ports import comports
import yaml
import os
import numpy as np
from builtins import input
from readchar import readkey
from datetime import datetime
import re
from time import sleep
from threading import Thread


class abort():
    def __init__(self):
        pass

# Note this program uses \n\r as a charriage return because of weirdness
# that was happening when some parts were running in threads and the carriage
# return was swallowed


def print_logo():
    print("\033c"
          r"==================================================="+"\n\r"
          r"  __  __           _           ___ _               "+"\n\r"
          r" |  \/  |         | |         |_  (_)              "+"\n\r"
          r" | .  . | ___  ___| |__ ______  | |___      _____  "+"\n\r"
          r" | |\/| |/ _ \/ __| '_ \______| | | \ \ /\ / / _ \ "+"\n\r"
          r" | |  | |  __/ (__| | | |   /\__/ / |\ V  V /  __/ "+"\n\r"
          r" \_|  |_/\___|\___|_| |_|   \____/|_| \_/\_/ \___| "+"\n\r"
          r"                                                   "+"\n\r"
          r"==================================================="+"\n\r\n\r")


def validate_output_dir(out_dir):
    """Check the out_dir is valid, creating dirs if needed
    The final format is  ~/Desktop/MechJiwe/
    """

    out_dir = os.path.expanduser(out_dir)

    try:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        return out_dir
    except:
        raise ValueError(
            "Error setting output directory.  Valid directories should either be exist or be creatable")


def clean_sample_id(samp_id):
    """Remove unwanted characters from sample ID for saving.
    Remove all characters than are not aphanumeric, 
    underscores, or hyphens. Spaces are replaced with
    underscores.
    """
    samp_id = re.sub(r' ', '_', samp_id)
    return re.sub(r'[^a-zA-Z0-9_\-]', '', samp_id)

def parameter_with_name(name, parameter_list):
    """Retrieve a parameter with the given name from a list"""
    for p in parameter_list:
        if p.name == name:
            return p
    raise KeyError("No parameter with the requested name was found.")


def control_tester_with_keyboard(mj, out_dir, samp_id):
    """Interactively control the tester using the keyboard"""

    global output_dir, sample_id
    output_dir = validate_output_dir(out_dir)
    sample_id = clean_sample_id(samp_id)
    cp = []  # this will hold the various parameters that can be adjusted.

    # First parameter adjusts step size
    step_param = kcu.InteractiveParameter(
        "step_size", 2**np.arange(14), initial_value=32)
    cp.append(step_param)

    # Next parameter allows you to move the tester
    def move_tester(step):
        mj.move(int(step * step_param.value))

    # This one will reset the load to zero
    def tare_tester():
        mj.tare()
        print_controls()
        print("\n\r\n\rLoad_cell tared\n\r")

    def print_load():
        print_controls()
        print("\n\r\n\rCurrent load: {}\n\r".format(mj.load()))

    # Provide a way to run the run_experiment command
    def run_experiment(experiment_data):
        global output_dir, sample_id
        try:
            print("Making measurement...Press t to terminate the run early\n\r")
            data = mj.run_experiment(experiment_data, show=True, verbose=True)
            if mj.abortrun:
                print("Terminated early\n\r")
            else:
                print("Done\n\r")

            filename = data['time_str']+"--"+data['sample_id']+".yaml"
            filename = os.path.join(output_dir, filename)
            with open(filename, "w") as outfile:
                yaml.dump(data, outfile)
            print("saved results to {}\n\r".format(filename))
        except Exception as e:
            print("Error: {}".format(e))

    # Adjust the output directory
    def adjust_output_dir():
        global output_dir
        new_output_dir = input("Please set the new output directory\n\r"
                               "Directories will be created if they don't exist.\n\r"
                               "New output directory: ")
        if len(new_output_dir) > 3:
            output_dir = validate_output_dir(new_output_dir)
        else:
            print('Directory must be more than three characters')
        print("\n\rOutput directory set to: %s\n\r" % output_dir)

    def print_output_dir():
        global output_dir
        return output_dir

    cp.append(kcu.FunctionParameter("Set output directory",
                                    adjust_output_dir, value_function=print_output_dir))

    # Adjust the sample id
    def adjust_sample_id():
        global sample_id
        new_sample_id = input("Please set the new sample ID.\n\r"
                              "This will be used for file names so\n\r"
                              "please stick to alpha-numeric characters, - and _\n\r"
                              "New sample ID: ")

        new_sample_id = clean_sample_id(new_sample_id)
        if len(new_sample_id) > 3:
            sample_id = new_sample_id
        else:
            print('Sample ID must be more than three characters')
            return False
        print("\n\rSample ID set to: %s\n\r" % sample_id)
        return True

    def print_sample_id():
        global sample_id
        return sample_id

    cp.append(kcu.FunctionParameter("Set sample ID",
                                    adjust_sample_id, value_function=print_sample_id))

    current_parameter = 0
    parameter = cp[current_parameter]

    def print_parameter(parameter, selected=False):
        if selected:
            select_state = '[*]  '
        else:
            select_state = '[ ]  '
        if parameter.name is not None:
            print("{}{}: {}".format(select_state, parameter.name, parameter.value))

    def print_controls():
        # \033c should clear the screen
        print_logo()
        print("up and down keys to move the tester\n\r"
              "(w and s also move the tester)\n\r"
              "m to enter the setup menu\n\r"
              "l to print the current load\n\r"
              "t to tare (zero) the load cell\n\r"
              "r to run a measurement\n\r"
              "q to quit")

    def print_menu(cp, selected):
        print_logo()
        print("Set-up Menu\n\r"
              "===========\n\r"
              "up and down (or w and s) to navigate\n\r"
              "+ and - to modify parameters\n\r"
              "q to exit menu\n\r"
              "\n\r")
        for n, parameter in enumerate(cp):
            print_parameter(parameter, n == selected)

    move_keys = {'w': 1, 's': -1, }

    def parameter_input(string, req_type, exit_strings=None):
        if exit_strings is None:
            exit_strings = ['q']
        success = False
        while not success:
            try:
                value = input(string)
                if value in exit_strings:
                    return abort()
                value = req_type(value)
                success = True
            except ValueError:
                pass
            except KeyboardInterrupt:
                return abort()
        return value

    def setup_measurement(ex_type):

        print('Entering measurement setup.\n\rAnswer "q" at any point to return to main menu\n\r\n\r')

        global sample_id

        experiment_data = {'tare': True,
                           'time_str': datetime.now().strftime("%Y%m%d_%H%M%S"),
                           'sample_id': sample_id
                           }
        if ex_type == 0:
            experiment_data['type'] = 'pull'
            experiment_data['auto_stop'] = True
        elif ex_type == 1:
            experiment_data['type'] = 'cycle'
            experiment_data['auto_stop'] = False
        elif ex_type == 2:
            experiment_data['type'] = 'increasing_cycle'
            experiment_data['auto_stop'] = False
        else:
            print('Unrecognised experiment type.\n\r')
            return None

        experiment_data['step_distance_mm'] = parameter_input(
            "Set step size between measurements in mm (positive=up) : ", float)
        if type(experiment_data['step_distance_mm']) is abort:
            return None

        experiment_data['total_distance_mm'] = parameter_input(
            "Set total distance in mm: ", float)
        if type(experiment_data['total_distance_mm']) is abort:
            return None

        if ex_type == 1 or ex_type == 2:
            experiment_data['number_cycles'] = parameter_input(
                "Number of cycles to perform: ", int)
            if type(experiment_data['number_cycles']) is abort:
                return None

        if ex_type == 2:
            experiment_data['number_increments'] = parameter_input(
                "Number of increments to perform: ", int)
            if type(experiment_data['number_increments']) is abort:
                return None

        experiment_data['number_averages'] = parameter_input(
            "Number of averages for each measurement: ", int)
        if type(experiment_data['number_averages']) is abort:
            return None

        experiment_data['time_per_point'] = parameter_input(
            "Time per point (enter nothing to run as fast as possible): ", float)
        if type(experiment_data['time_per_point']) is abort:
            return None

        return experiment_data

    # This is needed so someone doesn't freeze it by hitting a move key instantly
    sleep(2)
    print_controls()
    # make a pointless thread
    t = Thread(target=max, args=(1,))
    inMenu = False
    N_menu = len(cp)
    while True:
        c = readkey()
        if c == '\x03':  # quit
            break
        elif not inMenu:
            if not t.isAlive():
                if c in ['q']:  # quit
                    break
                elif c == 'l':
                    t = Thread(target=print_load)
                    t.start()
                elif c == 't':
                    t = Thread(target=tare_tester)
                    t.start()
                elif c == 'r':
                    if sample_id == 'Unknown':
                        while True:
                            choice = input(
                                'Sample ID not set, would you like to set the ID? [Y/n]: ')
                            if choice in ['n', 'N']:
                                break
                            elif choice in ['y', 'Y', '']:
                                changed = False
                                while not changed:
                                    changed = adjust_sample_id()
                                break
                    c = ''
                    while c not in ['\x03', 'p', 'c', 'i', 'q']:
                        print(
                            'Choose an experiment\n\rp: pull\n\rc: cycle\n\ri: increasing cycle\n\rq: return to main menu')
                        c = readkey()
                    if c == '\x03':  # quit
                        break
                    if c == 'q':
                        print_controls()
                    else:
                        # TODO define a variable rather than 0,1,2 for the types of experiments
                        if c == 'p':
                            params = setup_measurement(0)
                        if c == 'c':
                            params = setup_measurement(1)
                        if c == 'i':
                            params = setup_measurement(2)

                        if params is not None:
                            t = Thread(target=run_experiment, args=[params])
                            t.start()
                        else:
                            print_controls()
                elif c[0] == '\x1b':  # An arrow key
                    if c[2] == 'A':
                        t = Thread(target=move_tester, args=(1,))
                        t.start()
                    elif c[2] == 'B':
                        t = Thread(target=move_tester, args=(-1,))
                        t.start()
                elif c in list(move_keys.keys()):
                    t = Thread(target=move_tester, args=(move_keys[c],))
                    t.start()
                elif c == 'm':
                    print_menu(cp, current_parameter)
                    inMenu = True
            else:
                if c in ['t']:
                    mj.abortrun = True
        else:  # in the menu
            if c == 'q':  # quit menu
                inMenu = False
                print_controls()
            elif c in list(move_keys.keys()):
                d = move_keys[c]
                current_parameter = (current_parameter + N_menu - d) % N_menu
                print_menu(cp, current_parameter)
            elif c[0] == '\x1b':  # An arrow key
                if c[2] in ['A', 'B']:
                    if c[2] == 'A':
                        d = 1
                    elif c[2] == 'B':
                        d = -1
                    current_parameter = (
                        current_parameter + N_menu - d) % N_menu
                    print_menu(cp, current_parameter)
            elif c in ['+', '=', '\n\r']:  # change the current parameter up
                cp[current_parameter].change(1)
                print_menu(cp, current_parameter)
            elif c in ['-', '_']:      # or change it down
                cp[current_parameter].change(-1)
                print_menu(cp, current_parameter)


def parse_command_line_arguments():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
        description="Control the MechJiwe mechanical tester using keyboard commands")
    parser.add_argument(
        "--device", help="The serial port to use for communications")
    parser.add_argument(
        "--output", help="Directory for saved data", default="~/Documents/MechJiwe/")
    parser.add_argument(
        "--id", help="File ID. Output filename will be a time stamp and the ID", default="Unknown")
    args = parser.parse_args()
    return args


def keyboard_app():
    """An interactive script to control the tester with the keyboard"""
    args = parse_command_line_arguments()
    print_logo()
    device = args.device
    if device is None:
        print("Multiple devices found. Please select the number of the device to use.\n\r")
        ports = comports()
        for n, port in enumerate(ports):
            print("[%2d] %s\n\r" % (n, str(port)))
        valid = False
        N = len(ports)
        while not valid:
            try:
                dev = int(
                    input("Please input the number of the device to use: "))
                if dev < N and dev >= 0:
                    valid = True
            except ValueError:
                pass

        device = comports()[dev].device

    # Sometimes enumerating the com-ports seems to leave them open?
    serial.Serial(device).close()
    n = 0
    print("Connecting to tester using port {}\n\r".format(device))
    while True:
        try:
            sleep(1)

            mj = MechJiwe(device)
            break
        except OSError as e:
            n += 1
            if n > 10:

                print(
                    "Error connecting to the mechanical tester.  Perhaps an incorrect serial port?\n\r")
                print("Valid serial ports are: {}\n\r\n\r".format(
                    [c.device for c in comports()]))
                raise e
            else:
                print("Trying to connect.\n\r")

    print("\n\r\nInitalising... Please wait")
    control_tester_with_keyboard(mj, args.output, args.id)


if __name__ == "__main__":
    keyboard_app()

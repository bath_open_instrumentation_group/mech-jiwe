'''
This module is for data analysis only
'''

from copy import deepcopy
import yaml
from matplotlib import pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly


def _make_fancy(ax1, ax2, title):
    """
    Rather badly named helper function to clean up data run graphs
    """
    plt.sca(ax1)
    left, right = plt.xlim()
    plt.sca(ax2)
    plt.xlim((left, right))
    ax1.set_ylabel('Force [N]')
    ax2.set_ylabel('Residual [N]')
    ax2.set_xlabel('Position [mm]')
    ax1.set_title(title)
    ax1.xaxis.set_ticklabels([])
    ax1.tick_params(axis="x", bottom=True, top=True, direction='in')
    ax2.tick_params(axis="x", bottom=True, top=True, direction='in')
    ax1.grid(axis="x")
    ax2.grid(axis="x")
    plt.tight_layout()
    plt.subplots_adjust(hspace=0)


def fitting(position, load, load_std, order):
    """
    Performs and unweighted least squares fit using numpy polyfit.
    Some extra analysis is then performed and returned as a
    dictionary
    """
    # TODO: weight the fit!
    coef, cov = np.polyfit(position, load, order, cov=True)
    zero_pos = max(poly.polyroots(coef[::-1]))
    pos = position-zero_pos
    coef, cov = np.polyfit(pos, load, order, cov=True)
    coef_uncert = np.sqrt(np.diag(cov))
    fit_func = np.poly1d(coef)
    load_fit = fit_func(pos)
    resid = load_fit-load
    chi2_per_dof = np.sum((resid**2)/(load_std**2))/(len(load)-(order+1))
    return {'fit_type': 'Unweighted least squares',
            'fit_order': order,
            'coef': coef,
            'coef_uncert': coef_uncert,
            'zero_pos': zero_pos,
            'fit_points': load_fit,
            'residuals': resid,
            'chi2_per_dof': chi2_per_dof
            }


class DataRun():
    """
    This class stores the data from a single pupwards or downwards "run" of
    the Mech-Jiwe. DataRun objects are created for each run in an experiment
    automatically by MJFile objects.
    """

    ALL = 0
    LOADED = 1
    HIGHEST_SET = 2
    LOWEST_SET = 3

    def __init__(self, data_dict, mj_file):
        self._data = data_dict
        self._mj_file = mj_file
        self._valid = None
        self._selection = self.ALL
        self._subtract_unloaded_force = True
        self._set_valid(self._selection)

    @property
    def selection(self):
        """
        is a read/write property with allowable values of DataRun.ALL,
        DataRun.LOADED, DataRun.HIGHEST_SET, or DataRun.LOWEST_SET
        These define what data is returned by data properties such as
        `load` or `postion`.
        ALL - data properties return all data
        LOADED - returns all data except for areas where force is constant
                 implying the tester is unloaded in this region
        HIGHEST_SET - returns only the highest (in position) set of points,
                      with sets being defined as groups of loaded points
        LOWEST_SET - link HIGHEST_SET but data properties return the set of
                     points with the lowest position
        """
        return self._selection

    @selection.setter
    def selection(self, selection_value):
        assert selection_value in [
            self.ALL, self.LOADED, self.HIGHEST_SET, self.LOWEST_SET]
        self._set_valid(selection_value)
        self._selection = selection_value

    @property
    def subtract_unloaded_force(self):
        """
        Read/write boolean property. If True then load measurements
        have the unloaded force subtracted from them (Default True)
        """
        return self._subtract_unloaded_force

    @subtract_unloaded_force.setter
    def subtract_unloaded_force(self, in_val):
        assert isinstance(in_val, bool)
        self._subtract_unloaded_force = in_val

    @property
    def load(self):
        """
        Returns the datapoints for the load in Newtons.
        Which datapoints are returned will depend on the `selection` property.
        The force may be shifted in value by if `subtract_unloaded_force` is
            True
        """
        if self._subtract_unloaded_force:
            return self._data['load'][self._valid] - self._data['zero_force']
        return self._data['load'][self._valid]

    @property
    def load_std(self):
        """Returns the datapoints for the standard deviation of the load in Newtons.
        Which datapoints are returned will depend on the `selection` property.
        """
        return self._data['load_std'][self._valid]

    @property
    def position(self):
        """Returns the datapoints for the position in mm.
        Which datapoints are returned will depend on the `selection` property.
        """
        return self._data['position'][self._valid]

    def _set_valid(self, selection_value):
        assert selection_value in [
            self.ALL, self.LOADED, self.HIGHEST_SET, self.LOWEST_SET]
        if selection_value == self.ALL:
            self._valid = np.ones(self._data['load'].shape, dtype=bool)
        elif selection_value == self.LOADED:
            self._valid = self._data['data_point_loaded']
        elif selection_value == self.HIGHEST_SET:
            loaded = self._data['data_point_loaded']
            pos = self._data['position']
            self._valid = pos > np.max(pos[np.logical_not(loaded)])*1.0001
        elif selection_value == self.LOWEST_SET:
            loaded = self._data['data_point_loaded']
            pos = self._data['position']
            self._valid = pos < np.min(pos[np.logical_not(loaded)])*.999

    def fit_data(self, return_figure=False):
        """
        Returns a dictionary describing a fit to the data. The fit is either
        quadratic or linear depending on the Chi^2 of the two fits.
        """

        if len(self.position) < 2:
            grad = np.nan
            grad_u = np.nan
            return {}

        if len(self.position) == 2:
            grad = (self.load[0]-self.load[1]) / \
                (self.position[0]-self.position[1])
            grad_u = np.nan
            intercept = self.load[1] - grad*self.position[1]
            intercept_u = np.nan
            zero_pos = self.position[1] - self.load[1]/grad
            load_fit = grad*self.position+intercept
            resid = load_fit-self.load
            fit_params = {'fit_type': 'Estimated from 2 points',
                          'fit_order': 1,
                          'coef': [grad, intercept],
                          'coef_uncert': [grad_u, intercept_u],
                          'zero_pos': zero_pos,
                          'fit_points': load_fit,
                          'residuals': resid,
                          'chi2_per_dof': np.nan
                          }

        else:
            if len(self.position) < 5:
                fit_params = fitting(
                    self.position, self.load, self.load_std, 1)
            else:
                # Fit a linear and a quadratic. If the Chi^2 per degree of freedom for the
                # quadratic is less than 75% of what it was for linear then use quadratic
                fit_params1 = fitting(
                    self.position, self.load, self.load_std, 1)
                fit_params2 = fitting(
                    self.position, self.load, self.load_std, 2)
                if fit_params2['coef'][0] > 0:
                    # if the quadratic component is positive the analysis doesn't work well
                    # happens rarely on quite linear runs
                    fit_params = fit_params1
                else:
                    if fit_params2['chi2_per_dof'] < fit_params1['chi2_per_dof']*.75:
                        fit_params = fit_params2
                    else:
                        fit_params = fit_params1

        if return_figure:
            return fit_params, self._graph_analysis(fit_params)
        return fit_params

    def _graph_analysis(self, fit_params):
        fig = plt.figure(1, figsize=(8, 6))
        ax1 = fig.add_subplot(211)
        current_selection = self.selection
        self.selection = self.ALL
        ax1.errorbar(self.position, self.load, yerr=self.load_std, fmt='r*')
        if current_selection != self.ALL:
            self.selection = current_selection
            plt.errorbar(self.position, self.load, yerr=self.load_std, fmt='b.')
        if np.isnan(fit_params["coef_uncert"][-2]):
            grad_string = f'Gradient ~ {fit_params["coef"][-2]:.2f} N/mm'
        else:
            grad_string = (f'Gradient = {fit_params["coef"][-2]:.2f}'
                           f'+/-{fit_params["coef_uncert"][-2]:.2f} N/mm')

        title = (f'{grad_string}'
                 f' Point of unloading = {fit_params["zero_pos"]:.1f} mm')

        ax1.plot(self.position,
                 fit_params['fit_points'], 'k-',
                 label=f'chi^2 per dof = {fit_params["chi2_per_dof"]}')
        ax1.axvline(fit_params['zero_pos'], color='k')
        ax1.legend()
        ax2 = fig.add_subplot(212)
        ax2.plot(self.position, fit_params["residuals"], 'b.',)

        _make_fancy(ax1, ax2, title)

        return fig


class MJFile():
    """
    This class openes a data file from a full experiment on the MechJiwe
    The data is then loaded into a number of DataRun objects. One object
    for each set of movements in one direction.
    The only input to create a MJFile object is the filepath to the
    datafile
    """

    def __init__(self, filename):

        with open(filename, 'r') as stream:
            self._data = yaml.safe_load(stream)

        if 'runs' not in self._data:
            print('Warning: old mech-jiwe test software. Recreating experiement runs')
            self._build_runs()
            if self._data['end_condition'] != 'Experiment completed':
                print(
                    "Warning: As experiment didn't complete cannot check the experiment runs match")
        run_slices = []
        for run in self._data['runs']:
            run_slices.append(slice(run['start'], run['end']+1))
        self._data['runs_as_slices'] = run_slices
        self._calculate_if_loaded()
        self._selection = DataRun.ALL

    def _calculate_if_loaded(self):
        '''
        Estimates for each point whether the piece is uner load.
        This method needs some human evaluation to check if the reuslts make
        sense.
        '''

        # Get all points
        all_loads = np.asarray(self._data['load'])
        all_pos = np.asarray(self._data['position'])
        # Calculate gradient
        grad = np.diff(all_loads)/np.diff(all_pos)
        # Get the 1% with lowest gradient - may fail for smaller runs?
        unloaded_points = np.argsort(np.abs(grad))[0:len(grad)//100]
        unloaded = all_loads[unloaded_points]
        # Calculate the mean and std and exclude outliers points outside 3 std
        # Note as the data if the pin arm is used is normally bimodal then this is
        # then most points should fit very comfortable inside 3 std
        unloaded_mean = np.mean(unloaded)
        unloaded_std = np.std(unloaded, ddof=1)
        unloaded = unloaded[np.logical_and(unloaded > unloaded_mean-3*unloaded_std,
                                           unloaded < unloaded_mean+3*unloaded_std)]

        # Still cutting off at 3 std, this should get all unloaded points comfortably but will
        # cut off some data we want, this can be investigated later
        unloaded_mean = np.mean(unloaded)
        unloaded_std = np.std(unloaded, ddof=1)
        self._data['data_point_loaded'] = np.logical_or(all_loads < unloaded_mean-3*unloaded_std,
                                                        all_loads > unloaded_mean+3*unloaded_std)
        self._data['zero_force'] = unloaded_mean

    @property
    def data(self):
        """
        Returns a deepcopy of the data dictionary. This is all the data
        as saved by the MechJiwe. Some extra information for may be recreated
        for older files.
        """
        return deepcopy(self._data)

    def selection(self, selection_value):
        """
        Will set the selection value for all data runs. See
        DataRun.selection for more detail.
        """
        # TODO make this a property
        self._selection = selection_value

    def __len__(self):
        return len(self._data['runs'])

    def __getitem__(self, ind):
        run_slice = self._data['runs_as_slices'][ind]
        run_dict = {'load': np.asarray(self._data['load'][run_slice]),
                    'load_std': np.asarray(self._data['load_std'][run_slice]),
                    'position': np.asarray(self._data['position'][run_slice]),
                    'data_point_loaded': self._data['data_point_loaded'][run_slice],
                    'zero_force': self._data['zero_force']}
        run = DataRun(run_dict, self)
        run.selection = self._selection
        return run

    def _build_runs(self):
        """
        This function is to allow legacy files to open
        This is pretty much a copy of what is in the control software,
        but for old data it wasn't saved so this is to recreate it.
        Repeating the functionality here rather than calling the experiment
        methods so old files work even if methods update
        """

        point = 0
        runs = []

        assert self._data['type'] in ['pull', 'cycle', 'increasing_cycle']

        dist_mm = self._data['total_distance_mm']
        step_dist_mm = self._data['step_distance_mm']
        total_revs = dist_mm/self._data['tester']['dist_per_rev_mm']
        step_revs = step_dist_mm/self._data['tester']['dist_per_rev_mm']
        dist_steps = total_revs * self._data['tester']['steps_per_rev']
        step_size = int(step_revs * self._data['tester']['steps_per_rev'])

        if self._data['type'] == 'pull':
            num = int(abs(dist_steps//step_size))
            steps = [0]+[step_size]*num
            runs.append({'start': point,
                         'end': point+num})
            point += num

        elif self._data['type'] == 'cycle':

            n_cyc = self._data['number_cycles']
            num = int(abs(dist_steps//step_size))
            steps = [0]

            for n in range(n_cyc):
                steps += [step_size]*num
                runs.append({'start': point,
                             'end': point+num})
                point += num
                steps += [-step_size]*num
                runs.append({'start': point,
                             'end': point+num})
                point += num

        elif self._data['type'] == 'increasing_cycle':

            n_cyc = self._data['number_cycles']
            n_inc = self._data['number_increments']
            steps = [0]

            for n in range(n_inc):
                num = int(abs((dist_steps*(n+1)/n_inc)//step_size))
                for _cycle in range(n_cyc):
                    steps += [step_size]*num
                    runs.append({'start': point,
                                 'end': point+num})
                    point += num
                    steps += [-step_size]*num
                    runs.append({'start': point,
                                 'end': point+num})
                    point += num

        self._data['runs'] = runs

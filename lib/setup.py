 
__author__ = 'Julian Stirling'

from setuptools import setup, find_packages
import sys
from os import path



this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name = 'mechjiwe',
      version = '0.0.1',
      description = 'The python software for running the Mech-Jiwe mechanical tester',
      long_description = long_description,
      long_description_content_type='text/markdown',
      author = 'Julian Stirling',
      author_email = 'julian@julianstirling.co.uk',
      packages = find_packages(),
      
      keywords = ['Documentation','Hardware'],
      zip_safe = True,
      classifiers = [
          'Development Status :: 5 - Production/Stable',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3.6'
          ],
      install_requires=['pyyaml>=5.1','pyserial','numpy','readchar','matplotlib'],
      python_requires=">=3.6",
      entry_points = {'console_scripts': ['mechjiwe = mechjiwe.__main__:main']},
      )

